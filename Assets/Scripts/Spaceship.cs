using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    BulletManager bulletManager;
    public GameObject bulletObj;
    Rigidbody2D rigidbody2D;

    // speed the ship will move at
    const float speed = 750.0f;
    // amount to adjust the 'tilt' value of the device by
    const float acceleratorModifier = 0.5f;
    const float spaceshipMinScreenPos = 235.0f;
    const float spaceshipMaxScreenPos = 440.0f;
    Vector2 firingOffset = new Vector2(0.0f,0.0f);

    // bullet variables 
    int maxNumberOfBullets = 10;

    bool allowed = false;
    bool canFire = true;

    int playerHealth = 50;
    private bool isAlive;
    private bool canUpdate;

    // temporary variable used in update calculation
    Vector3 tempDir;

    // Start is called before the first frame update
    void Start()
    {
       // bulletObj.GetComponent<BoxCollider>().tag = "shipBullet";
        bulletManager = new BulletManager(maxNumberOfBullets, bulletObj);
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.freezeRotation = true;
        allowed = true;
        isAlive = true;
        canUpdate = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canUpdate)
        {
            // The reason we are gettin multiple bullets spawned is that we need more controls here
            // on the next iteration the touch count can still be > 0 and therefore firing will continue again
            if (Input.touchCount > 0 && canFire)
            {
                canFire = false;
                fire();
            }

            if (Input.touchCount == 0)
            {
                canFire = true;
            }

            if (allowed)
            {
                tempDir = calculateNewMovement();
            }
            else
            {
                if (Input.GetKeyUp(KeyCode.Space))
                {
                    canFire = false;
                    fire();
                }

                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    tempDir = Vector3.zero;
                    tempDir.y -= 0.001f;
                }

                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    tempDir = Vector3.zero;
                    tempDir.y += 0.001f;
                }

                if (Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.UpArrow))
                {
                    tempDir = Vector3.zero;
                }
            }
            //Debug.Log("GEORGE SHIP Y = " + transform.position.y);

            // Move the ship
            transform.Translate(tempDir * speed);

            // ensure the ship does not excede the limits of the screen
            transform.position = new Vector3(transform.position.x,
                Mathf.Clamp(transform.position.y, spaceshipMinScreenPos, spaceshipMaxScreenPos),
                transform.position.z);
        }
    }

    private Vector3 calculateNewMovement()
    {
        tempDir = Vector3.zero;

        // we need to adjust the calculations here so that the device can be used 
        // when it's parallel to the player rather than the ground
        float acceleration = Input.acceleration.y;
        acceleration += acceleratorModifier;

        tempDir.y = acceleration;

        // clamp acceleration vector to unit sphere
        if (tempDir.sqrMagnitude > 1)
            tempDir.Normalize();

        // Make it move it meters per second instead of meters per frame.
        tempDir *= Time.deltaTime;

        return tempDir;
    }
    
    private void fire()
    {
        firingOffset.x = transform.position.x + 40.0f;
        firingOffset.y = transform.position.y;
        bulletManager.getAvaliableBullet().GetComponent<Bullet>().activate(firingOffset);
    }

    private void OnCollisionEnter2D(UnityEngine.Collision2D collision)
    {
        rigidbody2D.velocity = Vector2.zero;
        if (collision.collider.gameObject.name.Contains("enemyBullet"))
        {
            playerHealth -= collision.collider.gameObject.GetComponent<Bullet>().damage;
            collision.collider.gameObject.GetComponent<Bullet>().kill();
        }

        if (playerHealth <= 0)
            die();
    }

    private void die()
    {
        isAlive = false;
        gameObject.SetActive(false);
    }

    public bool getIsAlive()
    {
        return isAlive;
    }

    public int getPlayerHealth()
    {
        return playerHealth;
    }

    public void disableUpdate()
    {
        canUpdate = false;
        bulletManager.deactivateBullets();
    }
}