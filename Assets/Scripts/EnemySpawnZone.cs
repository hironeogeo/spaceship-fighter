using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnZone : MonoBehaviour
{
    public Rect rect;
    public GameObject EnemyObj;

    public int maxEnemies;
    public int enemyBufferSize;
    private int spawnedEnemiesCount;
    private bool allEnemiesSpawned = false;

    float lastTime;
    float timer = 0.0f;
    float spawnFrequency;
    private int numEnemiesKilled;
    private bool canUpdate;

    List<GameObject> enemiesBuffer = new List<GameObject>();
    List<GameObject> deadEnemies = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        canUpdate = true;
        numEnemiesKilled = 0;
        spawnFrequency = 2.0f;
        spawnedEnemiesCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (canUpdate)
        {
            if (!allEnemiesSpawned)
            {
                if (timer <= spawnFrequency)
                {
                    timer += Time.deltaTime;
                }
                else if (timer > spawnFrequency && spawnedEnemiesCount < maxEnemies)
                {
                    timer = 0;
                    tryToSpawn();
                }
                else if (spawnedEnemiesCount == maxEnemies)
                {
                    allEnemiesSpawned = true;
                }
            }
        }
    }

    void tryToSpawn()
    { 
        if(spawnedEnemiesCount < enemyBufferSize)
        {
            GameObject tempEnemey = Instantiate(EnemyObj);
            tempEnemey.GetComponent<Enemy>().setupEnemy(gameObject);
            tempEnemey.GetComponent<Enemy>().activate(generateRandomEnemySpawnLoaction());
            enemiesBuffer.Add(tempEnemey);
            spawnedEnemiesCount += 1;
        }
        else
        {
            if(deadEnemies.Count > 0)
            {
                GameObject tempEnemey = deadEnemies[0];
                tempEnemey.GetComponent<Enemy>().setupEnemy(gameObject);
                tempEnemey.GetComponent<Enemy>().activate(generateRandomEnemySpawnLoaction());
                enemiesBuffer.Add(tempEnemey);
                deadEnemies.RemoveAt(0);
                spawnedEnemiesCount += 1;
            }
        }
    }

    void fillEnemyBuffer()
    {
        // we need to spawn enough enemies to fill our buffer
        for (int i = 0; i < enemyBufferSize; ++i)
        {
            GameObject tempEnemy = Instantiate(EnemyObj);
            tempEnemy.GetComponent<Enemy>().setupEnemy(gameObject);
            enemiesBuffer.Add(tempEnemy);
        }
    }
    
    void activateEnemy()
    {
        Vector2 newPos = generateRandomEnemySpawnLoaction();
        enemiesBuffer.Add(null);
        enemiesBuffer[enemyBufferSize] = enemiesBuffer[0];
        enemiesBuffer[0] = null; // this line may be unecessary
        enemiesBuffer.RemoveAt(0);
        enemiesBuffer[enemyBufferSize - 1].GetComponent<Enemy>().activate(newPos);
    }

    Vector2 generateRandomEnemySpawnLoaction()
    {
        Vector2 newLocation;
        newLocation.x = Random.Range(rect.xMin, rect.xMax);
        newLocation.y = Random.Range(rect.yMin, rect.yMax);
        return newLocation;
    }

    public void recordEnemyDeath(GameObject deadEnemy)
    {
        numEnemiesKilled += 1;
        deadEnemies.Add(deadEnemy);
    }

    public bool getAllEnemiesSpawned()
    {
        return allEnemiesSpawned;
    }

    public int getEnemiesCount()
    {
        return enemiesBuffer.Count;
    }

    public int getEnemyRemainingCount()
    {
        return maxEnemies - spawnedEnemiesCount;
    }

    public int getEnemiesNumKilled()
    {
        return numEnemiesKilled;
    }

    public void disableUpdate()
    {
        canUpdate = false;
        for (int i = 0; i < enemiesBuffer.Count; ++i)
        {
            enemiesBuffer[i].GetComponent<Enemy>().disableUpdate();
        }
    }
}