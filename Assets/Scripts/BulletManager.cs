using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    List<GameObject> bullets = new List<GameObject>();
    int maxBullets;
    GameObject bulletObj;

    public BulletManager(int totalMaxBullets, GameObject newBulletObj)
    {
        maxBullets = totalMaxBullets;
        bulletObj = newBulletObj;
    }

    bool canFire()
    {
        return false;
    }

    public int getNumActiveBullets()
    {
       // int max = bullets.Count;
        int numAlive = 0;

        for (int i = 0; i < bullets.Count; ++i)
        {
            if(bullets[i].GetComponent<Bullet>().isBulletAlive())
            {
                numAlive += 1;
            }
        }
        return numAlive;
    }

    // return an avaliable bullet that can be fired
    public GameObject getAvaliableBullet()
    {
        // if we haven't yet fired all of our bullets
        if(bullets.Count < maxBullets)
        {
            // add a new bullet and return that
            GameObject tempBullet = Instantiate(bulletObj);
            tempBullet.GetComponent<Bullet>().setupBullet();
            bullets.Add(tempBullet);
            return bullets[bullets.Count - 1];
        }
        else
        {
            // we've fired all of our bullets so we now need to recycle our oldest bullet
            bullets.Add(null);
            bullets[maxBullets] = bullets[0];
            bullets[0] = null; // this line may be unecessary
            bullets.RemoveAt(0);
            return bullets[maxBullets - 1];
        }
    }

    public void deactivateBullets()
    {
        for(int i = 0; i < bullets.Count; ++i)
        {
            bullets[i].GetComponent<Bullet>().setIsAlive(false);
        }
    }
}
