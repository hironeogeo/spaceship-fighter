using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bullet : MonoBehaviour
{
    public Sprite sprite;
    public int duration;
    public int damage;
    public float speed;
    public string parent;

    SpriteRenderer spriteRenderer;
    BoxCollider2D boxCollider2D;
    Rigidbody2D rigidbody2D;

    float lifeTimer;
    private bool isAlive;

    public void setupBullet()
    {
        isAlive = false;
        gameObject.SetActive(false);
        lifeTimer = 0;
        duration = 3000;
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        boxCollider2D.enabled = false;
        rigidbody2D.freezeRotation = true;
        spriteRenderer.sprite = sprite;
        spriteRenderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            if (lifeTimer <= duration)
            {
                lifeTimer += Time.deltaTime;
                
                Vector2 temp = Vector2.zero;
                temp.x = speed * Time.deltaTime;

                // move the bullet along
                transform.Translate(temp);
            }
            else
            {
                kill();
            }
        }
    }

    public bool isBulletAlive()
    {
        return isAlive;
    }

    public Sprite getBulletSprite()
    {
        return sprite;
    }

    public int getDuration()
    {
        return duration;
    }
    public float getSpeed()
    {
        return speed;
    }
    public int getDamage()
    {
        return damage;
    }

    public void kill()
    {
        isAlive = false;
        gameObject.SetActive(false);
        spriteRenderer.enabled = false;
        transform.position = Vector3.zero;
        boxCollider2D.enabled = false;
    }

    // a method to reset a dead bullet so it can be used again
    public void activate(Vector2 newPos)
    {
        // set new postion, duration and speed;
        isAlive = true;
        gameObject.SetActive(true);
        gameObject.transform.position = newPos;
        spriteRenderer.enabled = true;
        boxCollider2D.enabled = true;
        lifeTimer = 0.0f;
    }

    private void OnCollisionEnter2D(UnityEngine.Collision2D collision)
    {
        if (collision.gameObject.name.Contains("enemyBullet"))
        {
            kill();
            collision.collider.gameObject.GetComponent<Bullet>().kill();
        }
        else
        rigidbody2D.velocity = Vector2.zero;
        collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    public void setIsAlive(bool newAliveState)
    {
        isAlive = newAliveState;
    }
}
