using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject enemySpawning;
    public GameObject enemyRemainingObj;
    public GameObject playerHealthObj;
    public GameObject displayHHObj;
    public GameObject displayMMObj;
    public GameObject displaySSObj;

    public GameObject endGamePanel;
    public GameObject gameResultObj;
    public GameObject timePlayingObj;
    public GameObject enemiesKilledObj;
    public Button restartButton;
    public Button quitButton;

    bool isGamePlaying;
    int playerSurvivalTime;
    float newSecondMarker;
    Vector3Int timeStruct;

    // Start is called before the first frame update
    void Start()
    {
        isGamePlaying = true;
        playerSurvivalTime = 0;
        newSecondMarker = 0;
        timeStruct = Vector3Int.zero;

        endGamePanel.SetActive(false);

        restartButton.onClick.AddListener(restartGame);
        quitButton.onClick.AddListener(quitGame);
    }

    // Update is called once per frame
    void Update()
    {
        if (isGamePlaying)
        {
            if (newSecondMarker <= 1.0f)
            {
                newSecondMarker += Time.deltaTime;
            }
            else
            {
                newSecondMarker = 0;
                playerSurvivalTime += 1;
                timeStruct.z += 1;
            }

            if (timeStruct.z >= 60)
            {
                timeStruct.y += 1;
                timeStruct.z = 0;
            }

            if (timeStruct.y >= 60)
            {
                timeStruct.y = 0;
                timeStruct.x += 1;
            }

            enemyRemainingObj.GetComponent<Text>().text = enemySpawning.GetComponent<EnemySpawnZone>().getEnemyRemainingCount().ToString();
            playerHealthObj.GetComponent<Text>().text = player.GetComponent<Spaceship>().getPlayerHealth().ToString();
            displayHHObj.GetComponent<Text>().text = timeStruct.x.ToString();
            displayMMObj.GetComponent<Text>().text = timeStruct.y.ToString();
            displaySSObj.GetComponent<Text>().text = timeStruct.z.ToString();

            if (isGamePlaying)
            {
                testGameWin();
                testGameLoose();
            }
        }
    }

    void testGameWin()
    {
        if (player.GetComponent<Spaceship>().getIsAlive() &&
            enemySpawning.GetComponent<EnemySpawnZone>().getAllEnemiesSpawned() == true &&
            enemySpawning.GetComponent<EnemySpawnZone>().getEnemiesNumKilled() == enemySpawning.GetComponent<EnemySpawnZone>().maxEnemies)
        {
            Debug.Log("Game has been won");
            isGamePlaying = false;
            populateGameEndStats("You Won");
            endGamePanel.SetActive(true);
            disableGame();
        }
    }

    void testGameLoose()
    {
        if (player.GetComponent<Spaceship>().getIsAlive() == false)
        {
            Debug.Log("Game has been lost");
            isGamePlaying = false;
            populateGameEndStats("You Died");
            endGamePanel.SetActive(true);
            disableGame();
        }
    }

    void populateGameEndStats(string gameResultState)
    {
        gameResultObj.GetComponent<Text>().text = gameResultState;
        timePlayingObj.GetComponent<Text>().text = timeStruct.x.ToString() + ":" + timeStruct.y.ToString() + ":" + timeStruct.z.ToString();
        enemiesKilledObj.GetComponent<Text>().text = enemySpawning.GetComponent<EnemySpawnZone>().getEnemiesNumKilled().ToString();
    }

    void disableGame()
    {
        player.GetComponent<Spaceship>().disableUpdate();
        enemySpawning.GetComponent<EnemySpawnZone>().disableUpdate();
    }

    void restartGame()
    {
        SceneManager.LoadScene("Play_Scene");
    }

    void quitGame()
    {
        Application.Quit();
    }
}
