using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Sprite enemySprite;
    public GameObject bulletObj;
  
    SpriteRenderer spriteRenderer;
    BulletManager bulletManager;
    BoxCollider2D collider;
    Rigidbody2D rigidbody2D;
    GameObject enemySpawner;
  
    float moveTimer;
    float fireRate;
    float fireTimer;
    float maxMovingTime;
    float speed;
    bool isAlive;
    Vector2 firingOffset = new Vector2(0.0f, 0.0f);
    int maxNumberOfBullets = 5;
    int enemyHealth;

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            if (moveTimer <= maxMovingTime)
            {
                moveTimer += Time.deltaTime;
                Vector2 temp = Vector2.zero;
                temp.x = speed * Time.deltaTime;
                transform.Translate(temp);
            }
            else
            {
                moveTimer = 0;
                speed *= -1;
            }

            if(fireTimer <= fireRate)
            {
                fireTimer += Time.deltaTime;
            }
            else
            {
                fireTimer = 0;
                fire();
            }
        }
    }

    void FixedUpdate()
    {
        gameObject.transform.Translate(0.0f, 0.0f, 0.0f);
        gameObject.transform.Rotate(0.0f, 0.0f, 0.0f);
    }

    public void setupEnemy(GameObject spanwer)
    {
        enemySpawner = spanwer;
        isAlive = false;
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.freezeRotation = true;
        spriteRenderer.sprite = enemySprite;
        spriteRenderer.enabled = false;
        collider.enabled = false;
        bulletManager = new BulletManager(maxNumberOfBullets, bulletObj);
        maxMovingTime = 5.0f;
        speed = -30.0f;
        fireRate = 2.0f;
        enemyHealth = 0;
    }

    public void activate(Vector2 newPos)
    {
        // set new postion, duration and speed;
        isAlive = true;
        gameObject.SetActive(true);
        gameObject.transform.position = newPos;
        spriteRenderer.enabled = true;
        moveTimer = 0;
        fireTimer = 0;
        collider.enabled = true;
        enemyHealth = 100;
    }

    private void fire()
    {
        firingOffset.x = transform.position.x;
        firingOffset.y = transform.position.y;
        bulletManager.getAvaliableBullet().GetComponent<Bullet>().activate(firingOffset);
    }

    private void OnCollisionEnter2D(UnityEngine.Collision2D collision)
    {
        if (collision.collider.gameObject.name.Contains("shipBullet"))
        {
            enemyHealth -= collision.collider.gameObject.GetComponent<Bullet>().damage;
            collision.collider.gameObject.GetComponent<Bullet>().kill();
        }

        if (enemyHealth <= 0)
            die();
        rigidbody2D.velocity = Vector2.zero;
    }

    private void die()
    {
        // set new postion, duration and speed;
        isAlive = false;
        gameObject.SetActive(false);
        gameObject.transform.position = Vector3.zero;
        spriteRenderer.enabled = false;
        moveTimer = 0;
        fireTimer = 0;
        collider.enabled = false;
        enemyHealth = 0;
        enemySpawner.GetComponent<EnemySpawnZone>().recordEnemyDeath(gameObject);
      }

    public void disableUpdate()
    {
        isAlive = false;
        bulletManager.deactivateBullets();
    }
}
